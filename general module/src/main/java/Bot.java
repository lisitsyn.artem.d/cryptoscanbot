import com.google.common.base.CharMatcher;
import model.FigiEnum;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;

import java.util.*;

public class Bot extends TelegramLongPollingBot {
    private static final String TOKEN_BOT = "1863625976:AAHIupKwIFl9r0NGeNkdaqg4ydG-hXdYsRA";
    private static final String USERNAME_BOT = "demons_cryptoscanbot";

    private HashMap<String, HashMap<String, Thread>> cache = new HashMap<>();

    @Override
    public String getBotToken() {
        return TOKEN_BOT;
    }

    @Override
    public String getBotUsername() {
        return USERNAME_BOT;
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        String chatId = message.getChatId().toString();

        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(chatId);
        if (message != null && message.hasText()) {

            if (message.getText().startsWith("/sub") && message.getText().length() > 4) {  // ------------- подписка
                String figi = message.getText().substring(5);
                boolean isFigiMapped = false;
                for (FigiEnum fg : FigiEnum.values()) {
                    if (fg.name().equals(figi)) {
                        isFigiMapped = true;
                        if (cache.containsKey(chatId)) {    // если в hashmap уже есть такой чат (юзер)
                            if (cache.get(chatId).containsKey(figi)) {  // если у юзера уже активирована подписка
                                try {
                                    execute(sendMessage.setText("You can not subscribe on the same cryptocurrency twice."));
                                    return;
                                }
                                catch (TelegramApiException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        else { // если в hashmap нет такого чата (юзера)
                            cache.put(chatId, new HashMap<>());
                        }

                        if (!cache.get(chatId).containsKey(figi)) { // если у юзера не активирована подписка
                            try {
                                execute(sendMessage.setText("The subscription to the " + figi + " has been successfully activated"));
                            }
                            catch (TelegramApiException e) {
                                e.printStackTrace();
                            }

                            Thread run = new Thread(() -> {
                                while (true) {
                                    try {
                                        RestTemplate restTemplate = new RestTemplate();

                                        String url = "http://localhost:8080/figies/getfigi/" + figi;
                                        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
                                        String responseBody = response.getBody();
                                        try {
                                            JSONObject obj = new JSONObject(responseBody);

                                            String figii = obj.getString("figi");
                                            String name = obj.getString("name");
                                            double priceUsd = obj.getDouble("priceUSD");
                                            execute(sendMessage.setText("\n" + name + ": " + figii + " " + priceUsd + "$\n"));
                                        }
                                        catch (Exception exception) {
                                            System.err.println("failed to send REST request");
                                            exception.printStackTrace();
                                        }
                                        Thread.sleep(3000);
                                    }
                                    catch (InterruptedException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });
                            run.start();
                            cache.get(chatId).put(figi, run);
                        }


                    }
                }
                if (!isFigiMapped) {
                    try {
                        execute(sendMessage.setText("Wrong figi name of cryptocurrency.\nUse /all to see all cryptocurrencies."));
                        return;
                    }
                    catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }

            }
            else if (message.getText().startsWith("/unsub") && message.getText().length() > 6) {   // ------------------------- отписка
                String[] args = message.getText().split(" ");

                if (args.length != 1) {
                    String figi = args[1];
                    if (cache.containsKey(chatId)) {
                        if (cache.get(chatId).containsKey(figi)) {
                            cache.get(chatId).get(figi).stop();
                            cache.get(chatId).remove(figi);
                        }
                        try {
                            execute(sendMessage.setText("The subscription to the " + figi + " has been successfully disactivated"));
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (cache.containsKey(chatId) && !cache.get(chatId).containsKey(figi)) {
                        try {
                            execute(sendMessage.setText("There is not a cryptocurrency with this figi."));
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        try {
                            execute(sendMessage.setText("You have not any subscriptions yet"));
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else if (message.getText().startsWith("/watch") && message.getText().length() > 7) {
                String[] args = message.getText().split(" ");

                boolean isFigiMapped = false;
                for (FigiEnum fg : FigiEnum.values()) {
                    if (fg.name().equals(args[1])) {
                        isFigiMapped = true;
                    }
                }
                if (!isFigiMapped) {
                    try {
                        execute(sendMessage.setText("Wrong figi name of cryptocurrency.\nUse /all to see all cryptocurrencies."));
                        return;
                    }
                    catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                if (args.length > 2) {
                    double percentage = Double.parseDouble(args[2]);
                    if (percentage < 0 && percentage > 100) {
                        try {
                            execute(sendMessage.setText("You can not set the % less then 0 and more then 100."));
                            return;
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        try {
                            execute(sendMessage.setText("Now I watch the cryptocurrency " + args[1] + " for %" + percentage + " drop.\n" +
                                    "I will make you know when it will drop"));
                            RestTemplate restTemplate = new RestTemplate();

                            String urlCheck = "http://localhost:8080/figies/getfigi/" + args[1];
                            ResponseEntity<String> responseCheck = restTemplate.exchange(urlCheck, HttpMethod.GET, null, String.class);
                            String responseBodyCheck = responseCheck.getBody();
                            JSONObject objCheck = new JSONObject(responseBodyCheck);
                            double priceUsdLow = objCheck.getDouble("priceUSD") * ((100 - percentage) / 100);
                            double priceUsdStart = objCheck.getDouble("priceUSD");

                            Thread run = new Thread(() -> {
                                boolean isStopThread = false;
                                while (true) {
                                    try {
                                        RestTemplate restTemplatee = new RestTemplate();

                                        String url = "http://localhost:8080/figies/getfigi/" + args[1];
                                        ResponseEntity<String> response = restTemplatee.exchange(url, HttpMethod.GET, null, String.class);
                                        String responseBody = response.getBody();
                                        try {
                                            JSONObject obj = new JSONObject(responseBody);

                                            double priceUsd = obj.getDouble("priceUSD");
                                            String figii = obj.getString("figi");
                                            String name = obj.getString("name");
                                            if (priceUsd < priceUsdLow && !isStopThread) {
                                                execute(sendMessage.setText("\n" + name + ": " + figii + " has reached " + priceUsd + "$ with start price " + priceUsdStart + "$ fell by " + percentage + "%"));
                                                isStopThread = true;
                                            }

                                        }
                                        catch (Exception exception) {
                                            System.err.println("failed to send REST request");
                                            exception.printStackTrace();
                                        }
                                        Thread.sleep(5000);
                                    }
                                    catch (InterruptedException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            });
                            run.start();

                            return;
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    try {
                        execute(sendMessage.setText("Wrong command, write the percentage."));
                        return;
                    }
                    catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }
            else {
                switch (message.getText()) {
                    case "/start":
                        sendMessage.setReplyToMessageId(message.getMessageId());
                        try {
                            execute(sendMessage.setText("Hello there. I am a cryptobot. I have the useful information about cryptocurrencies."));
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "/help":
                        sendMessage.setReplyToMessageId(message.getMessageId());
                        try {
                            execute(sendMessage.setText("List of commands:\n" +
                                    "/all - to see the list of available cryptocurrencies for subscription\n" +
                                    "/sub 'figi name' - to subscribe to cryptocurrency\n" +
                                    "/unsub 'figi name' - to unsubscribe to cryptocurrency\n" +
                                    "/mysubs - to see all your subscriptions\n" +
                                    "/allunsub - to unsubscribe from all cryptocurrencies\n" +
                                    "/watch 'figi name' '% drop percentage' - be informed when drop in the rate of cryptocurrency from the current one by X%"));
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "/mysubs":
                        if (cache.containsKey(chatId) && !cache.get(chatId).isEmpty()) {
                            String[] keys = cache.get(chatId).keySet().toArray(new String[cache.get(chatId).keySet().size()]);
                            StringBuilder stringBuilder = new StringBuilder();
                            for (int i = 0; i < keys.length; i++) {
                                stringBuilder.append(keys[i] + " ");
                            }
                            try {
                                execute(sendMessage.setText("Your subscriptions: " + stringBuilder.toString()));
                            }
                            catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                execute(sendMessage.setText("You have not any subscriptions yet"));
                            }
                            catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    case "/all":
                        try {
                            RestTemplate restTemplate = new RestTemplate();

                            String url = "http://localhost:8080/figies/all";
                            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
                            String responseBody = response.getBody();
                            StringBuilder stringBuilder = new StringBuilder();
                            try {
                                JSONArray arr = new JSONArray(responseBody);
                                for (int i = 0; i < arr.length(); i++) {
                                    String name = arr.getJSONObject(i).getString("name");
                                    String figi = arr.getJSONObject(i).getString("figi");
                                    double priceUsd = (arr.getJSONObject(i).getDouble("priceUSD"));
                                    stringBuilder.append(name + ": " + figi + " " + priceUsd + "$\n");
                                }
                                execute(sendMessage.setText(String.valueOf(stringBuilder)));
                            }
                            catch (Exception exception) {
                                System.err.println("failed to send REST request");
                                exception.printStackTrace();
                            }
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "/allunsub":
                        if (cache.containsKey(chatId) && !cache.get(chatId).isEmpty()) {
                            for (FigiEnum fg : FigiEnum.values()) {
                                if (cache.get(chatId).containsKey(fg.name())) {
                                    cache.get(chatId).get(fg.name()).stop();
                                    cache.get(chatId).remove(fg.name());
                                }
                            }

                            try {
                                execute(sendMessage.setText("All subscriptions have been disactivated"));
                            }
                            catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            try {
                                execute(sendMessage.setText("You do not have any subscriptions yet."));
                            }
                            catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        }
                        break;
                    default:
                        sendMessage.setReplyToMessageId(message.getMessageId());
                        try {
                            execute(sendMessage.setText("Uknown command. Use /help to see the  list of commands"));
                        }
                        catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        }
    }

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new Bot());
        }
        catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
