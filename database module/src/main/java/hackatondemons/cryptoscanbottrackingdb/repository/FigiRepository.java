package hackatondemons.cryptoscanbottrackingdb.repository;

import hackatondemons.cryptoscanbottrackingdb.models.Figi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FigiRepository extends JpaRepository<Figi,Long> {
    List<Figi> findAll();
    Optional<Figi> findByFigi(String name);
}
