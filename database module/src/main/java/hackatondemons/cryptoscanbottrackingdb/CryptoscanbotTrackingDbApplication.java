package hackatondemons.cryptoscanbottrackingdb;

import hackatondemons.cryptoscanbottrackingdb.models.Figi;
import hackatondemons.cryptoscanbottrackingdb.repository.FigiRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringBootApplication
public class CryptoscanbotTrackingDbApplication {

    private static FigiRepository figiRepo;

    @Autowired
    private FigiRepository figiRepository;

    @PostConstruct
    public void init() {
        this.figiRepo = figiRepository;
    }


    public static void main(String[] args) {
        SpringApplication.run(CryptoscanbotTrackingDbApplication.class, args);
        Thread run = new Thread(() -> {
            while (true) {
                try {
                    RestTemplate restTemplate = new RestTemplate();

                    String url = "https://api.coincap.io/v2/assets?ids=bitcoin,ethereum,binance-coin,dogecoin,polkadot,cardano";
                    ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
                    String responseBody = response.getBody();
                    try {
                        JSONObject obj = new JSONObject(responseBody);
                        JSONArray arr = obj.getJSONArray("data");
                        for (int i = 0; i < arr.length(); i++) {
                            String figi = arr.getJSONObject(i).getString("symbol");
                            double priceUsd = Double.parseDouble(arr.getJSONObject(i).getString("priceUsd"));
                            Figi figiToUpdate = figiRepo.findByFigi(figi).orElseThrow(() -> new RuntimeException("Error, figi with name = " + figi + "  is not found"));
                            figiToUpdate.setPriceUSD(priceUsd);
                            figiRepo.save(figiToUpdate);
                        }
                    }
                    catch (Exception exception) {
                        System.err.println("failed to send REST request");
                        exception.printStackTrace();
                    }
                    Thread.sleep(10000);
                }
                catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });
        run.start();
    }

}
