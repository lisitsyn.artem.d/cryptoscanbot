package hackatondemons.cryptoscanbottrackingdb.controller;

import hackatondemons.cryptoscanbottrackingdb.models.Figi;
import hackatondemons.cryptoscanbottrackingdb.repository.FigiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/figies")
public class MainController {
    @Autowired
    FigiRepository figiRepository;

    @GetMapping("/all")
    public List<Figi> getAllFigies(){
        return figiRepository.findAll();
    }

    @GetMapping("/getfigi/{name}")
    public Figi getFigiByName(@PathVariable String name){
        return figiRepository.findByFigi(name).orElseThrow(() -> new RuntimeException("Error, figi with name = " + name +"  is not found"));
    }
}
